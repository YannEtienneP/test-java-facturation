package com.example.yep;

import com.example.yep.model.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class YepApplicationTests {

	// @Test void contextLoads() {}

	@Test
	// Test 1 : vérification des jours non facturés pour le service informatique entre le 2019-01-01 et le 2019-12-31
	public void testCalculerJoursNonFactures() {
		JoursNonFactures jnf = new JoursNonFactures("2019-01-01","2019-04-01", "informatique");
		String resultat_attendu = "[{\"service\":\"informatique\",\"dates\":[\"2019-02-26\",\"2019-02-27\",\"2019-02-28\"]}]";
		assertEquals(resultat_attendu,jnf.process());
	}

	@Test
	// Test 2 : vérification des jours non facturés pour le service commerce entre le 2019-01-01 et le 2019-12-31
	public void testCalculerJoursNonFacturesParService() {
		JoursNonFactures jnf = new JoursNonFactures("2019-01-01","2019-12-31", "commerce");
		String resultat_attendu = "[{\"service\":\"commerce\",\"dates\":[]}]";
		assertEquals(resultat_attendu,jnf.process());
	}

	@Test
	// Test 3 : vérification du prix moyen pour le service marketing entre le 2019-01-04 et le 2019-11-30
	public void testCalculerPrixMoyenFactures() {
		PrixMoyenFactures pmf = new PrixMoyenFactures("2019-01-04","2019-11-30", "marketing");
		String resultat_attendu = "[{\"service\":\"marketing\",\"prix_moyen\":\"299.79\"}]";
		assertEquals(resultat_attendu,pmf.process());
	}

	@Test
	// Test 4 : vérification du prix total pour tous les services entre le 2019-01-01 et le 2019-12-31
	public void testCalculerPrixTotalFactures() {
		PrixTotalFactures ptf = new PrixTotalFactures("2019-01-01","2019-12-31", "");
		String resultat_attendu = "[{\"service\":\"informatique\",\"prix_total\":\"2446.93\"},{\"service\":\"marketing\",\"prix_total\":\"2969.69\"},{\"service\":\"commerce\",\"prix_total\":\"1861.02\"}]";
		assertEquals(resultat_attendu,ptf.process());
	}

}
