package com.example.yep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YepApplication {
	// Fonction initiant l'API avec SpringBoot
	public static void main(String[] args) { SpringApplication.run(YepApplication.class, args);	}
}
