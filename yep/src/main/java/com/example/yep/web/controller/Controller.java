package com.example.yep.web.controller;

import com.example.yep.model.JoursNonFactures;
import com.example.yep.model.PrixMoyenFactures;
import com.example.yep.model.PrixTotalFactures;
import com.example.yep.model.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    public static class ObjetAPI{
        public String debut;
        public String fin;
        public String service = "";
    }

    // Jours non facturés
    @RequestMapping(value="/joursNonFactures", headers="Content-Type=application/json", method=RequestMethod.POST)
    @ResponseBody
    public String calculerJoursNonFactures(@RequestBody String requete) throws JsonProcessingException {
        ObjetAPI obj = new ObjectMapper().readValue(requete, ObjetAPI.class);
        JoursNonFactures facture = new JoursNonFactures(obj.debut, obj.fin, obj.service);
        return facture.process();
    }

    // Prix moyen des factures
    @RequestMapping(value="/prixMoyenFactures", headers="Content-Type=application/json", method=RequestMethod.POST)
    @ResponseBody
    public String calculerPrixMoyenFactures(@RequestBody String requete) throws JsonProcessingException {
        ObjetAPI obj = new ObjectMapper().readValue(requete, ObjetAPI.class);
        PrixMoyenFactures facture = new PrixMoyenFactures(obj.debut, obj.fin, obj.service);
        return facture.process();
    }

    // Prix total des factures par service
    @RequestMapping(value="/prixTotalFactures", headers="Content-Type=application/json", method=RequestMethod.POST)
    @ResponseBody
    public String calculerPrixTotalFacturesParService(@RequestBody String requete) throws JsonProcessingException {
        ObjetAPI obj = new ObjectMapper().readValue(requete, ObjetAPI.class);
        PrixTotalFactures facture = new PrixTotalFactures(obj.debut, obj.fin, obj.service);
        return facture.process();
    }

}
