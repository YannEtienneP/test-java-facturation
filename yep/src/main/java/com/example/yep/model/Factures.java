package com.example.yep.model;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.text.SimpleDateFormat;
import java.math.RoundingMode;
import java.text.DecimalFormat;

// Classe parente des trois API
public class Factures {
    protected String date_debut_str = "";
    protected Date date_debut;
    protected String date_fin_str = "";
    protected Date date_fin;
    protected String service;
    protected List<String> services = new ArrayList<String>();
    protected String data_source = "factures.csv";
    protected List<String> data = new ArrayList<String>();
    protected DecimalFormat decimal_format;

    public Factures(String date_debut, String date_fin, String service){
        this.date_debut_str = date_debut;
        this.date_fin_str = date_fin;
        this.service = service;
    }

    // Lecture des données depuis le fichier CSV, stockage des entrées dans une liste
    public void getData(){
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(this.data_source).getFile());
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String line = myReader.nextLine();
                data.add(line);
            }
            data.remove(0);
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    // Vérification des dates en entrée d'API : la première doit être antérieure à la seconde
    public boolean checkDates(){
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(this.date_debut_str);
            this.date_debut = date1;
            Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(this.date_fin_str);
            this.date_fin = date2;
            if (date2.compareTo(date1) >= 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    // Liste des services concernés, par défaut : tous les services référencés dans la liste d'entrées
    public void listServices(){
        for (String line : this.data){
            String[] tab = line.split(",");
            if (!this.services.contains(tab[0])){
                this.services.add(tab[0]);
            }
        }
    }

    // Paramètrage du format des valeurs numériques
    public void setDecimalFormat(){
        Locale currentLocale = Locale.getDefault();
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
        otherSymbols.setDecimalSeparator('.');
        this.decimal_format = new DecimalFormat("0.00", otherSymbols);
        this.decimal_format.setRoundingMode(RoundingMode.UP);
    }

}
