package com.example.yep.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.*;
import java.text.SimpleDateFormat;

// Classe de l'API n°1 : Statistiques sur les jours non facturés pendant une période de temps, par ordre chronologique,
//   avec possibilité de filtrer par service.
public class JoursNonFactures extends Factures{

    public JoursNonFactures(String date_debut, String date_fin, String service){
        super(date_debut, date_fin, service);
    }

    // Classe intermédiaire pour formater les données avant conversion en JSON dans la réponse
    static class Jours{
        public String service;
        public ArrayList<String> dates;
        public Jours(String service, ArrayList<String> jours){
            this.service = service;
            this.dates = jours;
        }
    }

    public String process(){
        ArrayList<Jours> jours = new ArrayList<>();
        if (this.checkDates()){
            this.setDecimalFormat();
            this.getData();
            if (!this.service.equals("")){
                this.services.add(this.service);
            }
            else {
                this.listServices();
            }
            try {
                long interval = (this.date_fin.getTime() - this.date_debut.getTime())/86400000;
                List<String> days = new ArrayList<String>();
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                c.setTime(this.date_debut);
                days.add(sdf.format(c.getTime()));
                for (int i=0; i<interval; i++){
                    c.add(Calendar.DAY_OF_YEAR, 1);
                    days.add(sdf.format(c.getTime()));
                }
                for (String service : this.services) {
                    ArrayList<String> days_not_billed = new ArrayList<String>(days);
                    for (String day : days) {
                        for (String line : this.data) {
                            String[] tab = line.split(",");
                            if (tab[0].equals(service)) {
                                Date date1 = sdf.parse(tab[2]);
                                Date date2 = sdf.parse(tab[3]);
                                if (date1.compareTo(sdf.parse(day)) <= 0 && date2.compareTo(sdf.parse(day)) >= 0) {
                                    days_not_billed.remove(day);
                                }
                            }
                        }
                    }
                    jours.add(new Jours(service, days_not_billed));
                }
                ObjectMapper mapper = new ObjectMapper();
                return mapper.writeValueAsString(jours);
            } catch (Exception e) {
                e.printStackTrace();
                return "{}";
            }
        }
        else {
            return "{}";
        }
    }
}
