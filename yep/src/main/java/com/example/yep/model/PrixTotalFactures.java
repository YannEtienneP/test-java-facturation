package com.example.yep.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

// Classe de l'API n°3 : Statistiques sur le prix total pendant une période de temps, groupées par service
public class PrixTotalFactures extends Factures {

    public PrixTotalFactures(String date_debut, String date_fin, String service){
        super(date_debut, date_fin, service);
    }

    // Classe intermédiaire pour formater les données avant conversion en JSON dans la réponse
    static class PrixTotal{
        public String service;
        public String prix_total;
        public PrixTotal(String service, String prix_total){
            this.service = service;
            this.prix_total = prix_total;
        }
    }

    public String process(){
        ArrayList<PrixTotal> totaux = new ArrayList<>();
        if (this.checkDates()){
            this.setDecimalFormat();
            this.getData();
            this.listServices();
            try {
                for (String service : this.services) {
                    float total = 0;
                    for (String line : this.data) {
                        String[] tab = line.split(",");
                        if (tab[0].equals(service)) {
                            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(tab[2]);
                            Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(tab[3]);
                            if ((date1.compareTo(this.date_debut) <= 0 && date2.compareTo(this.date_debut) >= 0) ||
                                    (date1.compareTo(this.date_fin) <= 0 && date2.compareTo(this.date_fin) >= 0) ||
                                    (date1.compareTo(this.date_debut) <= 0 && date2.compareTo(this.date_fin) >= 0) ||
                                    (date1.compareTo(this.date_debut) >= 0 && date2.compareTo(this.date_fin) <= 0)) {
                                total += Float.parseFloat(tab[4]);
                            }
                        }
                    }
                    totaux.add(new PrixTotal(service, decimal_format.format(total)));
                }
                ObjectMapper mapper = new ObjectMapper();
                return mapper.writeValueAsString(totaux);
            } catch (Exception e) {
                e.printStackTrace();
                return "{}";
            }
        }
        return "{}";
    }
}
