package com.example.yep.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.fasterxml.jackson.databind.ObjectMapper;

// Classe de l'API n°2 : Statistiques sur le prix moyen des factures pendant une période de temps avec possibilité de filtrer par service.
public class PrixMoyenFactures extends Factures {

    public PrixMoyenFactures(String date_debut, String date_fin, String service){
        super(date_debut, date_fin, service);
    }

    // Classe intermédiaire pour formater les données avant conversion en JSON dans la réponse
    static class PrixMoyen{
        public String service;
        public String prix_moyen;
        public PrixMoyen(String service, String prix_moyen){
            this.service = service;
            this.prix_moyen = prix_moyen;
        }
    }

    public String process() {
        ArrayList<PrixMoyenFactures.PrixMoyen> pm = new ArrayList<>();
        if (this.checkDates()) {
            this.setDecimalFormat();
            this.getData();
            if (!this.service.equals("")) {
                this.services.add(this.service);
            } else {
                this.listServices();
            }
            try {
                for (String service : this.services) {
                    float total = 0;
                    int cpt = 0;
                    for (String line : this.data) {
                        String[] tab = line.split(",");
                        if (tab[0].equals(service)) {
                            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(tab[2]);
                            Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(tab[3]);
                            if ((date1.compareTo(this.date_debut) <= 0 && date2.compareTo(this.date_debut) >= 0) ||
                                    (date1.compareTo(this.date_fin) <= 0 && date2.compareTo(this.date_fin) >= 0) ||
                                    (date1.compareTo(this.date_debut) <= 0 && date2.compareTo(this.date_fin) >= 0) ||
                                    (date1.compareTo(this.date_debut) >= 0 && date2.compareTo(this.date_fin) <= 0)) {
                                total += Float.parseFloat(tab[4]);
                                cpt++;
                            }
                        }
                    }
                    pm.add(new PrixMoyen(service, (decimal_format.format(total / cpt)).toString()));
                }
                ObjectMapper mapper = new ObjectMapper();
                return mapper.writeValueAsString(pm);
            } catch (Exception e) {
                e.printStackTrace();
                return "{}";
            }
        }
        return "{}";
    }
}
