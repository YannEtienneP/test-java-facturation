+-----------+
|  READ-ME  |
+-----------+


Description : test de d�veloppement Java, statistiques sur les facturations.


Version : v20.08.31


Pr�requis :
 - Java 8 ou plus
 - Spring-boot 2.2.X
 - Spring
 - JUnit 5
 - Maven 3.5 ou plus


Lancement de l'API :

Se positionner � la racine du projet : cd yep
Lancer l'application avec Maven : mvn spring-boot:run
L'application s'initialise sur le port 8029.


Exemples d'appel de l'API en ligne de commande :

curl --header "Content-Type: application/json" -d "{\"debut\":\"2019-01-01\", \"fin\":\"2019-12-31\", \"service\":\"informatique\"}" http://localhost:8029/joursNonFactures

curl --header "Content-Type: application/json" -d "{\"debut\":\"2019-01-01\", \"fin\":\"2019-12-31\", \"service\":\"commerce\"}" http://localhost:8029/joursNonFactures

curl --header "Content-Type: application/json" -d "{\"debut\":\"2019-01-04\", \"fin\":\"2019-11-30\", \"service\":\"marketing\"}" http://localhost:8029/prixMoyenFactures

curl --header "Content-Type: application/json" -d "{\"debut\":\"2019-01-01\", \"fin\":\"2019-12-31\", \"service\":\"\"}" http://localhost:8029/prixTotalFactures


